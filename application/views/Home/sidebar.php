<div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
              <div class="menu_section">
                <h3>General</h3>
                <ul class="nav side-menu">
                  <li><a href = "<?php echo site_url()?>/welcome"><i class="fa fa-home"></i> Home </a>
                  </li>
                  <li><a><i class="fa fa-edit"></i> Master <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="<?php echo base_url();?>index.php/c_anggota">Anggota</a></li>                   
                      <li><a href="<?php echo base_url();?>index.php/c_rak">Rak</a></li>
                      <li><a href="<?php echo base_url();?>index.php/c_pengarang">Pengarang</a></li>
                      <li><a href="<?php echo base_url();?>index.php/c_penerbit">Penerbit</a></li>
                      <li><a href="<?php echo base_url();?>index.php/c_reg_buku">Registrasi Buku</a></li>
                      <li><a href="form_upload.html">Buku</a></li>
                    </ul>
                  </li>
                  <li><a><i class="fa fa-desktop"></i> Transaksi <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="general_elements.html">Peminjaman</a></li>
                      <li><a href="media_gallery.html">Pengembalian</a></li>
                    </ul>
                  </li>
                </ul>
              </div>

            </div>