<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	public function index()
	{
		$data['content'] = 'Content/content';
		$data['title'] = 'Home';
		$this->load->view('Home/index',$data);
	}
}
